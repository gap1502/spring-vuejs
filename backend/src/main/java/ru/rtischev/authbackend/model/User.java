package ru.rtischev.authbackend.model;


import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceCreator;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;



@ToString
@Table(name = "usr")
public class User {

    @Getter
    @Id
    private Long id;

    @Getter @Setter
    private String username;

    @Getter @Setter
    private String email;

    @Getter @Setter
    private String password;

    @Getter @Setter
    private String tfaSecret;
    @MappedCollection
    private final Set<Token> tokens = new HashSet<>();
    @MappedCollection
    private final Set<PasswordRecovery> passwordRecoveries = new HashSet<>();


    public static User of(String username, String email, String password) {
        return new User(null, username, email, password, null, Collections.emptyList(), Collections.emptyList());
    }

    @PersistenceCreator
    private User(Long id, String username, String email, String password, String tfaSecret, Collection<Token> tokens, Collection<PasswordRecovery> passwordRecoveries) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.tfaSecret = tfaSecret;
        this.tokens.addAll(tokens);
        this.passwordRecoveries.addAll(passwordRecoveries);
    }

    public void addToken(Token token) {
        this.tokens.add(token);
    }

    public Boolean removeToken(Token token) {
        return this.tokens.remove(token);
    }

    public Boolean removeTokenIf(Predicate<? super Token> predicate) {
        return this.tokens.removeIf(predicate);
    }

    public void addPasswordRecovery(PasswordRecovery passwordRecovery) {
        this.passwordRecoveries.add(passwordRecovery);
    }

    public Boolean removePasswordRecovery(PasswordRecovery passwordRecovery) {
        return this.passwordRecoveries.remove(passwordRecovery);
    }

    public Boolean removePasswordRecoveryIf(Predicate<? super PasswordRecovery> predicate) {
        return this.passwordRecoveries.removeIf(predicate);
    }
}
